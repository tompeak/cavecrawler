//
//  ViewController.swift
//  8BallMagic
//
//  Created by Tom Peak on 27.04.18.
//  Copyright © 2018 Tom Peak. All rights reserved.
//

import UIKit
import SceneKit
import CoreMotion

class ViewController: UIViewController {
    var scnView: SCNView!
    var scnScene: SCNScene!
    var cameraNode: SCNNode!
    
    var drawData = [SCNVector3]()
    var indices:Data = Data(bytes: [])
    var source:SCNGeometryElement!
    var speed:Float = 0.3
    var progress:Float = 0
    var step:Int = 0;
    var oldRight:Float = 0.5
    var oldLeft:Float = 0.5
    let height:Float = 1.7;
    let byteSize = MemoryLayout<CInt>.size

    var birdView:Bool = false;

    var building:Bool = false
    
    let rad:Float = Float(Double.pi/180)
    
    //Camera start
    let x:Float = -2.5
    var y:Float = -11
    let z:Float = 4.2
    
    var plan:SCNGeometrySource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupScene()
        setupCamera()
        addLight()
        
        let zoomGesture = UIPinchGestureRecognizer(target: self, action: #selector(ZoomAction(sender:)))
        view.addGestureRecognizer(zoomGesture)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(PanAction(sender:)))
        view.addGestureRecognizer(panGesture)
        
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(RotateAction(sender:)))
        view.addGestureRecognizer(rotateGesture)
        
       
        
        srand48(time(UnsafeMutablePointer<Int>(bitPattern: 0))) // Random Seed
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func setupView() {
        
        scnView = self.view as! SCNView
        
        //scnView.showsStatistics = true
        
        //scnView.allowsCameraControl = true
        
        //scnView.autoenablesDefaultLighting = true
        
    }
    
    func setupScene() {
        
        scnScene = SCNScene()
        
        //scnView.delegate = self
        
        scnView.backgroundColor = UIColor.black
        
        scnView.scene = scnScene
        
        
    }
    
    func setupCamera() {
        
        cameraNode = SCNNode()
        
        cameraNode.camera = SCNCamera()
        
        cameraNode.position = SCNVector3(x: x, y: y, z: z)
        cameraNode.rotation = SCNVector4Make(80*rad,1*rad, -40*rad,1)
        cameraNode.camera?.fieldOfView =  80.0
        scnScene.rootNode.addChildNode(cameraNode)
    }
    
    func Generate3DStackFloor(left:Float,right:Float,stack:Int){
        var positions = [SCNVector3]()
        positions.append(SCNVector3Make(-left, progress+speed,  0.0))
        positions.append(SCNVector3Make( -oldLeft,progress,  0.0))
        positions.append(SCNVector3Make(right, progress+speed,  0.0))
        positions.append(SCNVector3Make(oldRight, progress,  0.0))
        
        //let bytes:[CInt] = [step*3, step*3+1, step*3+2, step*3+1, step*3+2, step*3]
        let bytes:[CInt] = [0, 1, 2, 1, 3, 2]
        indices = (NSMutableData(bytes: bytes, length:bytes.count * byteSize) as Data)
        source = SCNGeometryElement(data: indices , primitiveType: .triangles, primitiveCount: 2, bytesPerIndex:byteSize)
        plan = SCNGeometrySource(vertices: positions)
        
        let  floor = SCNGeometry(sources: [plan],elements: [source])
        let floorMat = SCNMaterial()
        floorMat.diffuse.contents = UIColor(red: 1, green: 1, blue: 0, alpha: 0)
        floor.materials=[floorMat]
        
        let geometryNode = SCNNode(geometry: floor)
        scnScene.rootNode.addChildNode(geometryNode)
        let delay = stack * 200;
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            SCNTransaction.begin()
            let ease = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            SCNTransaction.animationTimingFunction = ease
            floorMat.diffuse.contents = UIColor(red: 0.99, green: 0.6, blue: 0.97, alpha: 1)
            SCNTransaction.commit()
        }
    }
    
    func Generate3DStackLeft(left:Float,stack:Int){
        var positions = [SCNVector3]()
        positions.append(SCNVector3Make(-left, progress+speed,  0.0))
        positions.append(SCNVector3Make( -oldLeft,progress,  0.0))
        positions.append(SCNVector3Make(-left, progress+speed, height))
        positions.append(SCNVector3Make(-oldLeft, progress,  height))
     
        //let bytes:[CInt] = [step*3, step*3+1, step*3+2, step*3+1, step*3+2, step*3]
        let bytes:[CInt] = [0, 1, 2, 1, 2, 3]
        indices = (NSMutableData(bytes: bytes, length:bytes.count * byteSize) as Data)
        source = SCNGeometryElement(data: indices , primitiveType: .triangles, primitiveCount: 2, bytesPerIndex: byteSize)
        
        plan = SCNGeometrySource(vertices: positions)
        
        let  floor = SCNGeometry(sources: [plan],elements: [source])
        let floorMat = SCNMaterial()
        floorMat.isDoubleSided = true
        //floorMat.fillMode = SCNFillMode.lines
        floorMat.transparency = 0.6
        floorMat.diffuse.contents = UIColor(red:1, green: 1, blue: 0, alpha: 0)
        floor.materials=[floorMat]
        
        let geometryNode = SCNNode(geometry: floor)
        scnScene.rootNode.addChildNode(geometryNode)
        
        let delay = stack * 200;
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.2
            let ease = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            SCNTransaction.animationTimingFunction = ease
            floorMat.diffuse.contents = UIColor(red:1, green: 1, blue: 1, alpha: 1)
            SCNTransaction.commit()
        }
    }
    
    func Generate3DStackRight(right:Float,stack:Int){
        var positions = [SCNVector3]()
        positions.append(SCNVector3Make(right, progress+speed,  0.0))
        positions.append(SCNVector3Make(oldRight,progress,  0.0))
        positions.append(SCNVector3Make(right, progress+speed,  height))
        positions.append(SCNVector3Make(oldRight, progress,  height))
      
        //let bytes:[CInt] = [step*3, step*3+1, step*3+2, step*3+1, step*3+2, step*3]
        let bytes:[CInt] = [0, 1, 2, 1, 2, 3]
        
        indices = (NSMutableData(bytes: bytes, length:bytes.count * 4) as Data)
        source = SCNGeometryElement(data: indices , primitiveType: .triangles, primitiveCount: 2, bytesPerIndex:byteSize)
        plan = SCNGeometrySource(vertices: positions)
        
        let  floor = SCNGeometry(sources: [plan],elements: [source])
        let floorMat = SCNMaterial()
        floorMat.isDoubleSided = true
        floorMat.diffuse.contents = UIColor(red: 1, green:  1, blue: 0, alpha: 0)
        floor.materials=[floorMat]
        
        let geometryNode = SCNNode(geometry: floor)
        scnScene.rootNode.addChildNode(geometryNode)
        
        let delay = stack * 200;
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.2
            let ease = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            SCNTransaction.animationTimingFunction = ease
             floorMat.diffuse.contents = UIColor(red: 0.7, green: 0.7, blue: 0.97, alpha: 1)
            SCNTransaction.commit()
        }
    }
    
    func newScan(){
        cameraNode.removeAllActions()
        for move:Int in 0 ... 7 {
            
            // !!! Here should be the data from the robot. The treshhold can be calculized for the StackPoints on delay.
            let left:Float = Float(drand48()*0.6 + 0.3) // StackPoint - relative to Robot - Maschine learning: What's known or different.
            let right:Float = Float(drand48()*0.6 + 0.3)
        
            Generate3DStackFloor(left:left,right:right,stack:move)
            Generate3DStackRight(right: right,stack:move)
            Generate3DStackLeft(left: left,stack:move)
            
            // save data for Image Export
            drawData.append( SCNVector3Make(left, right, speed))
            
            oldLeft = left
            oldRight = right
            step = step + 1
            if step%7 == 0 {
                addLight()
            }
            // go forward
            progress += speed
            // set new range
            speed = Float(drand48() * 0.4 + 0.5);
        }
        // move cam
        let shakeHead = SCNAction.rotateBy(x:CGFloat(25 * rad),y:0,z: CGFloat(20.0 * rad), duration: 0.5)
        let waitHead = SCNAction.rotateBy(x:0.0,y:CGFloat(-2 * rad),z:CGFloat(15.0 * rad),duration:0.6)
        let shakeBack = SCNAction.rotateTo(x:CGFloat(60 * rad),y:CGFloat(1*rad), z:CGFloat(-30.0 * rad),duration:0.3)
        let moveCenter = SCNAction.move(to: SCNVector3Make(-0.1,progress + y,1.5), duration: 0.8)
        let moveAbove = SCNAction.move(to: SCNVector3Make(0,progress + y - 4  ,height*3), duration: 0.2)
        let moveBack = SCNAction.move(to: SCNVector3Make(x,progress + y ,z), duration: 0.5)
        
        shakeHead.timingMode = .easeIn
        moveCenter.timingMode = .easeOut
        moveBack.timingMode = .easeOut
        waitHead.timingMode = .easeInEaseOut
        
        let shakeSequence:SCNAction
        
        let moveSequence:SCNAction
        // for stress test
        if building {
            moveSequence = SCNAction.sequence([moveCenter,moveAbove,moveBack])
            shakeSequence = SCNAction.sequence([waitHead,waitHead.reversed(),shakeBack])
            
        } else {
            shakeSequence = SCNAction.sequence([shakeHead,waitHead,waitHead.reversed(),waitHead,shakeBack])
            moveSequence = SCNAction.sequence([moveAbove,moveCenter,moveCenter,moveCenter,moveBack])
            building = true;
        }
        
        cameraNode.runAction(shakeSequence)
        cameraNode.runAction(moveSequence){
            self.building = false
        }
    }
    
    func addLight(){
        let lightNode = SCNNode()
        
        lightNode.light = SCNLight()
        
        lightNode.position = SCNVector3Make(0, progress + 1,z + 9)
        lightNode.rotation = SCNVector4Make(1,0,0, (Float(arc4random_uniform(50))-25) * rad)
        lightNode.light?.intensity = 600
        //lightNode.light?.castsShadow = true
        //lightNode.light?.shadowMode = SCNShadowMode(rawValue: 0)!
        lightNode.light?.type = SCNLight.LightType.spot
    
        scnScene.rootNode.addChildNode(lightNode)
    }
    
    @IBAction func ExportImagePressed(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(DrawCave(startingImage: UIImage(named: "plainexport")!, data: drawData,factor:800/progress), self, #selector(SaveCave(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @IBAction func PressScan(_ sender: Any) {
        if birdView {
            // move back, start a new scan
            let delay = 1.3;
            let shakeHead = SCNAction.rotateTo(x:CGFloat(80.0 * rad),y:CGFloat(1 * rad), z:CGFloat(-40 * rad), duration:0.6)
            let moveAbove = SCNAction.move(to: SCNVector3Make(x,progress + y,height*2), duration: 0.5)
            
            
            shakeHead.timingMode = .easeIn
            moveAbove.timingMode = .easeIn;
            
            let shakeSequence = SCNAction.sequence([shakeHead])
            let moveSequence = SCNAction.sequence([moveAbove])
            
            cameraNode.runAction(moveSequence)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.cameraNode.runAction(shakeSequence)

            }
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                self.newScan()
            }
            birdView = false;
        }
        else {
            newScan()
        }
    }
    
    @IBAction func BirdviewPressed(_ sender: Any) {
        let shakeHead = SCNAction.rotateTo(x:0,y:0,z: 0, duration: 0.5)
        let moveCenter = SCNAction.move(to: SCNVector3Make(-0.3,progress + y + 0.5,z), duration: 0.4)
        let moveAbove = SCNAction.move(to: SCNVector3Make(0,((progress) / 2.0) ,progress*0.7), duration: 1)
        
        shakeHead.timingMode = .easeInEaseOut
        moveCenter.timingMode = .easeInEaseOut;
        
        let shakeSequence = SCNAction.sequence([shakeHead])
        let moveSequence = SCNAction.sequence([moveCenter,moveAbove])
        cameraNode.runAction(shakeSequence)
        cameraNode.runAction(moveSequence)
        birdView = true
    }
    
    @objc func RotateAction(sender: UIRotationGestureRecognizer) {
        if(sender.state == UIGestureRecognizerState.ended) {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.2
            let ease = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            SCNTransaction.animationTimingFunction = ease
            cameraNode.rotation = SCNVector4Make(0.0, 0.0, 1 , 0.1 * Float(sender.velocity) +  Float(cameraNode.rotation.w))
            SCNTransaction.commit()
        }
        // debugPrint(Float(sender.velocity),"Vel")
    }
 
    @objc func PanAction(sender: UIPanGestureRecognizer) {
        if(sender.state == UIGestureRecognizerState.ended) {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.2
            let ease = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            SCNTransaction.animationTimingFunction = ease
            let translation = sender.translation(in: self.view)
            cameraNode.position = SCNVector3Make(cameraNode.position.x - Float(translation.x) * 0.01, cameraNode.position.y - Float(translation.y)*0.01, cameraNode.position.z)
            SCNTransaction.commit()
        }
    }
    
    @objc func ZoomAction(sender: UIPinchGestureRecognizer) {
        if(sender.state == UIGestureRecognizerState.ended) {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.2
            let ease = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            SCNTransaction.animationTimingFunction = ease
            let zoom:Float = Float(sender.scale) > 1.0 ? -2.0 - cameraNode.position.z * 0.2 : 2.0 + cameraNode.position.z * 0.2
            cameraNode.position = SCNVector3Make(cameraNode.position.x, cameraNode.position.y, cameraNode.position.z + zoom )
            SCNTransaction.commit()
        }
        //debugPrint(sender.scale)
    }
    
    @objc func SaveCave(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
}
/*extension ViewController: SCNSceneRendererDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime timeS: TimeInterval) {
        
        
    }

 }*/
