//
//  Exporter.swift
//  CaveCrawler
//
//  Created by Tom Peak on 28.04.18.
//  Copyright © 2018 Tom Peak. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

func DrawCave(startingImage: UIImage,data:[SCNVector3],factor:Float) -> UIImage {
    
    UIGraphicsBeginImageContext(startingImage.size)

    startingImage.draw(at: CGPoint.zero)

    let context = UIGraphicsGetCurrentContext()!
    
    context.setStrokeColor(UIColor.white.cgColor)
    
    let floorColor =  UIColor(red: 0.99, green: 0.6, blue: 0.97, alpha: 0.7)
    
    context.setFillColor(floorColor.cgColor)
    context.setAlpha(1)
    context.setLineWidth(1.0)
    var exportProgress = 0
    context.move(to: CGPoint(x: Int(200.0-data[0].x * factor), y:exportProgress))
    for i in 1 ... data.count-1{
        exportProgress += Int(data[i].z*factor)
        context.addLine(to: CGPoint(x: Int(200.0 - data[i].x * factor), y:exportProgress))
    }
    context.addLine(to: CGPoint(x:Int(200.0 + data[data.count-1].y * factor), y:exportProgress))
    for i in  1 ... data.count {
        exportProgress -= Int(data[data.count-i].z*factor)
        context.addLine(to: CGPoint(x: Int(200.0 + data[data.count-i].y * factor), y:exportProgress))
        
    }
    context.addLine(to: CGPoint(x: Int(200.0 + data[0].y * factor), y:exportProgress))
    context.drawPath(using: .fillStroke)
    
    let map = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
  
    return map!
}

